import requests
import math
import time
import os

def deg2num(lat_deg, lon_deg, zoom):
	lat_rad = math.radians(lat_deg)
	n = 2.0 ** zoom
	xtile = int((lon_deg + 180.0) / 360.0 * n)
	ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
	return (xtile, ytile)

def num2deg(xtile, ytile, zoom):
	n = 2.0 ** zoom
	lon_deg = xtile / n * 360.0 - 180.0
	lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
	lat_deg = math.degrees(lat_rad)
	return (lat_deg, lon_deg)

if __name__=="__main__":



	wardbbox = (-1.113224,50.7823884,-1.0780334,50.8050669)
	wardzoom = 18

	tx1, ty1 = deg2num(wardbbox[1], wardbbox[0], wardzoom)
	tx2, ty2 = deg2num(wardbbox[3], wardbbox[2], wardzoom)

	print (tx1, ty1, tx2+1, ty2-1)

	for tx in range(tx1, tx2+2):
		for ty in range(ty1, ty2-2, -1):

			print (tx, ty)
			imgPth = "{}/{}/{}.png".format(wardzoom, tx, ty)
			if os.path.exists(imgPth): continue

			url = "https://map.fosm.org/default/{}/{}/{}.png".format(wardzoom, tx, ty)
			r = requests.get(url)
			assert r.status_code == 200

			pth = "{}/{}".format(wardzoom, tx)
			os.makedirs(pth, exist_ok=True)
			
			fi = open(imgPth, "wb")
			fi.write(r.content)
			fi.close()

			time.sleep(1)

