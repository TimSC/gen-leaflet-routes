import csv
from pyproj import Proj, transform
import numpy as np

if __name__=="__main__":

	rows = csv.DictReader(open("/home/tim/dev/clusteraddress/st-thomas-routes-v1.csv", "rt"))

	routes = {}
	outProj = Proj(init='epsg:4326')
	inProj = Proj(init='epsg:27700')

	for li in rows:
		route = int(li['route'])
		if route in routes:
			routes[route].append(li)
		else:
			routes[route] = [li]
	

	out = csv.writer(open("wardbbox.csv", "wt"))
	for route, rows in routes.items():

		pts = []

		for li in rows:
			pts.append((float(li['Eastings']), float(li['Northings'])))

		pts = np.array(pts)
		bbox = (np.min(pts[:,0]), np.min(pts[:,1]), np.max(pts[:,0]), np.max(pts[:,1]))

		x1,y1 = transform(inProj,outProj,bbox[0],bbox[1])
		x2,y2 = transform(inProj,outProj,bbox[2],bbox[3])
		bbox2 = (x1, y1, x2, y2)

		print (route, bbox, bbox2)
		out.writerow([route]+list(bbox)+list(bbox2))
	
	del out

