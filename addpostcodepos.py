import zipfile
import csv
import encodings

if __name__=="__main__":

	data = zipfile.ZipFile("codepo_gb.zip")
	#print (data.namelist())
	data2 = csv.reader(encodings.utf_8.StreamReader(data.open("Data/CSV/po.csv", "r")))
	
	pcdict = {}
	for li in data2:
		pcdict[li[0]] = (float(li[2]), float(li[3]))

	fi = "/home/tim/Documents/green-st-thomas-routes/st-thomas-routes.csv"
	addrs = csv.DictReader(open(fi, "rt"))
	print (addrs.fieldnames)
	fiOut = "/home/tim/Documents/green-st-thomas-routes/st-thomas-routes-pos.csv"
	addrsOut = csv.DictWriter(open(fiOut, "wt"), fieldnames=addrs.fieldnames+['Eastings', 'Northings'])

	addrsOut.writeheader()
	for li in addrs:
		pc = li['Postcode']
		li['Eastings'] = pcdict[pc][0]
		li['Northings'] = pcdict[pc][1]
		addrsOut.writerow(li)

	del addrsOut

