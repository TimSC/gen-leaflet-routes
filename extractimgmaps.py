import os
import csv
from downloadimgtiles import deg2num, num2deg
from PIL import Image, ImageFont, ImageDraw
from pyproj import Proj, transform

def MergeImgTiles(zoom, tx1, ty1, tx2, ty2): # Inclusive of these ti;e numbers
	
	merged = Image.new('RGB', (256*(tx2+1-tx1), 256*(ty1+1-ty2)))

	for tx in range(tx1, tx2+1):
		for ty in range(ty1, ty2-1, -1):
			pth = "{}/{}/{}.png".format(zoom, tx, ty)
			assert os.path.exists(pth)
			img = Image.open(pth).convert("RGB")
			

			offset = (256*(tx-tx1), 256*(ty-ty2))
			merged.paste(img, offset)

	return merged

def AddMarginGb(bbox, margin): #metres

	# Add margin in gb projection
	gbx1,gby1 = transform(gpsProj,gbProj,bbox[0],bbox[1])
	gbx2,gby2 = transform(gpsProj,gbProj,bbox[2],bbox[3])

	bbox2 = (gbx1, gby1, gbx2, gby2)
	bbox3 = (bbox2[0]-margin, bbox2[1]-margin, bbox2[2]+margin, bbox2[3]+margin)
	
	gpsx1,gpsy1 = transform(gbProj,gpsProj,bbox3[0],bbox3[1])
	gpsx2,gpsy2 = transform(gbProj,gpsProj,bbox3[2],bbox3[3])

	bbox4 = (gpsx1, gpsy1, gpsx2, gpsy2)
	return bbox4

if __name__=="__main__":

	routes = csv.reader(open("wardbbox.csv", "rt"))
	zoom = 18
	gpsProj = Proj(init='epsg:4326')
	gbProj = Proj(init='epsg:27700')
	font = ImageFont.truetype("FreeMono.ttf", 20)

	for li in routes:

		bbox = tuple(map(float, li[5:]))

		bbox4 = AddMarginGb(bbox, 100)

		tx1, ty1 = deg2num(bbox4[1], bbox4[0], zoom)
		tx2, ty2 = deg2num(bbox4[3], bbox4[2], zoom)
		
		#print (li[0], tx1, ty1, tx2, ty2)

		img = MergeImgTiles(zoom, tx1, ty1, tx2, ty2)

		draw = ImageDraw.Draw(img)
		draw.text((10, img.size[1]-30), "© fosm.org and OpenStreetMap contributors, CC BY-SA 2.0", "#f00", font)
		del draw

		img.save("route{}.png".format(li[0]))

	

